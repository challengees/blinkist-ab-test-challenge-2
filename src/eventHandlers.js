import { trackEvent, trackPageview } from "./lib/analytics-api.js";

const ctaHandler = (page, fingerprint, bucket) => {
    const eventParams = {
        page,
        fingerprint,
        bucket,
        event: 'signup-start'
    }

    trackEvent(eventParams);
}
const signupHandler = (page, fingerprint, bucket) => {
    const form = document.getElementById('signup_form');
    const formData = new FormData(form);
    const email = formData.get('email');

    const eventParams = {
        page,
        fingerprint,
        bucket,
        email,
        event: 'signup-verification-pending'
    }

    trackEvent(eventParams);
}
const visibilityChangeHandler = (page, fingerprint, bucket) => {
    const event = document.visibilityState === 'hidden' ? 'focus-off' : 'focus-on'
    const eventParams = {
        page,
        fingerprint,
        bucket,
        event
    }

    trackEvent(eventParams);
}
const loadHandler = (page, fingerprint, bucket) => {
    trackPageview({ page, fingerprint, bucket });
}

export const registerEventHandlers = (page, fingerprint, bucket) => {
    const CTA = document.querySelector('a[data-type="CTA"]');
    if (CTA !== null) {
        CTA.addEventListener('click', ctaHandler.bind(this, page, fingerprint, bucket));
    }

    const signupButton = document.querySelector('a[data-type="sign_up"]');
    if (signupButton !== null) {
        signupButton.addEventListener('click', signupHandler.bind(this, page, fingerprint, bucket));
    }

    document.addEventListener(
        'visibilitychange',
        visibilityChangeHandler.bind(this, page, fingerprint, bucket)
    );

    window.addEventListener('load', loadHandler.bind(this, page, fingerprint, bucket));
}