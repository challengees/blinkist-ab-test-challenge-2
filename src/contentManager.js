export const showContent = (bucket) => {
    // In real-world scenario, this would pull content from CDN server

    const elements = document.querySelectorAll(`[data-type="test"][data-variant="${bucket}"]`);
    elements.forEach(element => {
        element.removeAttribute('hidden');
    });
}

