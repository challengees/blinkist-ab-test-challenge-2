import { showContent } from "./contentManager.js";
import { registerEventHandlers } from "./eventHandlers.js";

const NUMBER_OF_BUCKETS = 2; // This should be loaded from some changeable configuration place
let fingerprint = 0;
try {
    fingerprint = getBrowserFingerprint({ hardwareOnly: true });
}catch{
    console.error('Can you please turn off your ad blocker...please :)');
}
const page = document.location.pathname;
const fingerprintNumber = parseInt(fingerprint);
const bucket = fingerprintNumber % NUMBER_OF_BUCKETS;

showContent(bucket);
registerEventHandlers(page, fingerprint, bucket);